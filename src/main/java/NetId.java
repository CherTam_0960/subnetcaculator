/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.JTextField;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author kitti
 */
public class NetId {

    private int octet1;
    private int octet2;
    private int octet3;
    private int octet4;
    private int mask;
    private int mask128 = 10000000;
    private int mask192 = 11000000;
    private int mask224 = 11100000;
    private int mask240 = 11110000;
    private int mask248 = 11111000;
    private int mask252 = 11111100;
    private int mask254 = 11111110;
    private int mask255 = 11111111;

    public NetId(int Octet1, int Octet2, int Octet3, int Octet4, int mask) {
        this.octet1 = Octet1;
        this.octet2 = Octet2;
        this.octet3 = Octet3;
        this.octet4 = Octet4;
        this.mask = mask;
    }

    public int getMask() {
        return mask;
    }

    public void setMask(int mask) {
        this.mask = mask;
    }

    public int getOctet1() {
        return octet1;
    }

    public void setOctet1(int Octet1) {
        this.octet1 = Octet1;
    }

    public int getOctet2() {
        return octet2;
    }

    public void setOctet2(int Octet2) {
        this.octet2 = Octet2;
    }

    public int getOctet3() {
        return octet3;
    }

    public void setOctet3(int Octet3) {
        this.octet3 = Octet3;
    }

    public int getOctet4() {
        return octet4;
    }

    public void setOctet4(int Octet4) {
        this.octet4 = Octet4;
    }

    @Override
    public String toString() {
        return "IP address : " + octet1 + "." + octet2 + "." + octet3 + "." + octet4;
    }

    public String convertNetworkaddress(int octet4) {
        String StringOctet4 = Integer.toBinaryString(this.octet4);
        int octConverted = Integer.parseInt(StringOctet4);

        String subnet = "";
        String wild = "";
        String binarySub = "";

        switch (mask) {
            case 1:
                subnet = "<html>Subnet mask : 128.0.0.0";
                wild = "<br><br>Wild mask :127.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 10000000 00000000 00000000 00000000</html>";
                break;
            case 2:
                subnet = "<html>Subnet mask : 192.0.0.0";
                wild = "<br><br>Wild mask :63.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11000000 00000000 00000000 00000000</html>";
                break;
            case 3:
                subnet = "<html>Subnet mask : 224.0.0.0";
                wild = "<br><br>Wild mask :31.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11100000 00000000 00000000 00000000</html>";
                break;
            case 4:
                subnet = "<html>Subnet mask : 240.0.0.0";
                wild = "<br><br>Wild mask :15.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11110000 00000000 00000000 00000000</html>";
                break;
            case 5:
                subnet = "<html>Subnet mask : 248.0.0.0";
                wild = "<br><br>Wild mask :7.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111000 00000000 00000000 00000000</html>";
                break;
            case 6:
                subnet = "<html>Subnet mask : 252.0.0.0";
                wild = "<br><br>Wild mask :3.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111100 00000000 00000000 00000000</html>";
                break;
            case 7:
                subnet = "<html>Subnet mask : 254.0.0.0";
                wild = "<br><br>Wild mask :1.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111110 00000000 00000000 00000000</html>";
                break;
            //class A
            case 8:
                subnet = "<html>Subnet mask : 255.0.0.0";
                wild = "<br><br>Wild mask :0.255.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 00000000 00000000 00000000</html>";
                break;
            case 9:
                subnet = "<html>Subnet mask : 255.128.0.0";
                wild = "<br><br>Wild mask :0.127.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 10000000 00000000 00000000</html>";
                break;
            case 10:
                subnet = "<html>Subnet mask : 255.192.0.0";
                wild = "<br><br>Wild mask :0.63.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11000000 00000000 00000000</html>";
                break;
            case 11:
                subnet = "<html>Subnet mask : 255.224.0.0";
                wild = "<br><br>Wild mask :0.31.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11100000 00000000 00000000</html>";
                break;
            case 12:
                subnet = "<html>Subnet mask : 255.240.0.0";
                wild = "<br><br>Wild mask :0.15.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11110000 00000000 00000000</html>";
                break;
            case 13:
                subnet = "<html>Subnet mask : 255.248.0.0";
                wild = "<br><br>Wild mask :0.7.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111000 00000000 00000000</html>";
                break;
            case 14:
                subnet = "<html>Subnet mask : 255.252.0.0";
                wild = "<br><br>Wild mask :0.3.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111100 00000000 00000000</html>";
                break;
            case 15:
                subnet = "<html>Subnet mask : 255.254.0.0";
                wild = "<br><br>Wild mask :0.1.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111110 00000000 00000000</html>";
                break;
            case 16:
//class B
                subnet = "<html>Subnet mask : 255.255.0.0";
                wild = "<br><br>Wild mask :0.0.255.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 00000000 00000000</html>";
                break;
            case 17:
                subnet = "<html>Subnet mask : 255.255.128.0";
                wild = "<br><br>Wild mask :0.0.127.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 10000000 00000000</html>";
                break;
            case 18:
                subnet = "<html>Subnet mask : 255.255.192.0";
                wild = "<br><br>Wild mask :0.0.63.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11000000 00000000</html>";
                break;
            case 19:
                subnet = "<html>Subnet mask : 255.255.224.0";
                wild = "<br><br>Wild mask :0.0.31.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11100000 00000000</html>";
                break;
            case 20:
                subnet = "<html>Subnet mask : 255.255.240.0";
                wild = "<br><br>Wild mask :0.0.15.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11110000 00000000</html>";
                break;
            case 21:
                subnet = "<html>Subnet mask : 255.255.248.0";
                wild = "<br><br>Wild mask :0.0.7.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111000 00000000</html>";
                break;
            case 22:
                subnet = "<html>Subnet mask : 255.255.252.0";
                wild = "<br><br>Wild mask :0.0.3.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111100 00000000</html>";
                break;
            case 23:
                subnet = "<html>Subnet mask : 255.255.254.0";
                wild = "<br><br>Wild mask :0.0.1.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111110 00000000</html>";
                break;
//class C
            case 24:
                subnet = "<html>Subnet mask : 255.255.255.0";
                wild = "<br><br>Wild mask :0.0.0.255";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  00000000</html>";
                break;
            case 25:
                subnet = "<html>Subnet mask : 255.255.255.128";
                wild = "<br><br>Wild mask :0.0.0.127";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  10000000</html>";
                break;
            case 26:
                subnet = "<html>Subnet mask : 255.255.255.192";
                wild = "<br><br>Wild mask :0.0.0.31";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  11000000</html>";
                break;
            case 27:
                subnet = "<html>Subnet mask : 255.255.255.224";
                wild = "<br><br>Wild mask :0.0.0.15";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  11100000</html>";
                break;
            case 28:
                subnet = "<html>Subnet mask : 255.255.255.240";
                wild = "<br><br>Wild mask :0.0.0.7";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  11110000</html>";
                break;
            case 29:
                subnet = "<html>Subnet mask : 255.255.255.248";
                wild = "<br><br>Wild mask :0.0.0.3";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  11111000</html>";
                break;
            case 30:
                subnet = "<html>Subnet mask : 255.255.255.252";
                wild = "<br><br>Wild mask :0.0.0.1";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  11111100</html>";
                break;
            case 31:
                subnet = "<html>Subnet mask : 255.255.255.254";
                wild = "<br><br>Wild mask :0.0.0.127";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  11111110</html>";
                break;
            case 32:
                subnet = "<html>Subnet mask : 255.255.255.255";
                wild = "<br><br>Wild mask :0.0.0.0";
                binarySub = "<br><br>Binary Subnet Mask : 11111111 11111111 11111111  11111111</html>";
                break;
        }
        return subnet + wild + binarySub;
    }

    public String showCIRD() {
        return "CIRD notation : /" + mask;
    }

    String showType(int firstOctet, int secondOctet) {
        if (firstOctet == 10) {
            return "IP type : Private";
        } else if (firstOctet == 172) {
            if (secondOctet == 16) {
                return "IP type : Private";
            } else if (secondOctet == 17) {
                return "IP type : Private";
            } else if (secondOctet == 18) {
                return "IP type : Private";
            } else if (secondOctet == 19) {
                return "IP type : Private";
            } else if (secondOctet == 20) {
                return "IP type : Private";
            } else if (secondOctet == 21) {
                return "IP type : Private";
            } else if (secondOctet == 22) {
                return "IP type : Private";
            } else if (secondOctet == 23) {
                return "IP type : Private";
            } else if (secondOctet == 24) {
                return "IP type : Private";
            } else if (secondOctet == 25) {
                return "IP type : Private";
            } else if (secondOctet == 26) {
                return "IP type : Private";
            } else if (secondOctet == 27) {
                return "IP type : Private";
            } else if (secondOctet == 28) {
                return "IP type : Private";
            } else if (secondOctet == 29) {
                return "IP type : Private";
            } else if (secondOctet == 30) {
                return "IP type : Private";
            } else if (secondOctet == 31) {
                return "IP type : Private";
            } else {
                return "IP type : Public";
            }
        } else if (firstOctet == 192 && secondOctet == 168) {
            return "IP type : Private";
        } else {
            return "IP type : Public";
        }
    }

    String showShort() {
        return "IP address : " + octet1 + "." + octet2 + "." + octet3 + "." + octet4 + " /" + mask;
    }

    String showBinaryID() {
        return "Binary ID : " + toBinary(octet1) + toBinary(octet2) + toBinary(octet3) + toBinary(octet4);
    }

    public static String toBinary(int x) {
        return StringUtils.leftPad(Integer.toBinaryString(x), 8, '0');
    }

    String showIntegerID() {
        String binaryStr = toBinary(octet1) + toBinary(octet2) + toBinary(octet3) + toBinary(octet4);
        long intID = Long.parseLong(binaryStr, 2);
        return "Integer ID : " + intID;
    }

    String showHexID() {
        String binaryStr = toBinary(octet1) + toBinary(octet2) + toBinary(octet3) + toBinary(octet4);
        long intID = Long.parseLong(binaryStr, 2);
        String hexID = Integer.toHexString((int) intID);
        return "Hex ID : 0x" + hexID;
    }

    String showClass(int firstOctet) {
        if (firstOctet <= 127) {
            return "IP class : A";
        } else if (firstOctet >= 128 && firstOctet <= 191) {
            return "IP class : B";
        } else if (firstOctet >= 192 && firstOctet <= 223) {
            return "IP class : C";
        }
        return "IP class : NA";
    }

    String showNetAddress(int Octet1, int Octet2, int Octet3, int Octet4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octet1 = Integer.parseInt(toBinary(Octet1));
        octet2 = Integer.parseInt(toBinary(Octet2));
        octet3 = Integer.parseInt(toBinary(Octet3));
        octet4 = Integer.parseInt(toBinary(Octet4));
        switch (mask) {
            case 1:
                mask1 = 128;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 2:
                mask1 = 192;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 3:
                mask1 = 224;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 4:
                mask1 = 240;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 5:
                mask1 = 248;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 6:
                mask1 = 252;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 7:
                mask1 = 254;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            //class A
            case 8:
                mask1 = 255;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 9:
                mask1 = 255;
                mask2 = 128;
                mask3 = 0;
                mask4 = 0;
                break;
            case 10:
                mask1 = 255;
                mask2 = 192;
                mask3 = 0;
                mask4 = 0;
                break;
            case 11:
                mask1 = 255;
                mask2 = 224;
                mask3 = 0;
                mask4 = 0;
                break;
            case 12:
                mask1 = 255;
                mask2 = 240;
                mask3 = 0;
                mask4 = 0;
                break;
            case 13:
                mask1 = 255;
                mask2 = 248;
                mask3 = 0;
                mask4 = 0;
                break;
            case 14:
                mask1 = 255;
                mask2 = 252;
                mask3 = 0;
                mask4 = 0;
                break;
            case 15:
                mask1 = 255;
                mask2 = 254;
                mask3 = 0;
                mask4 = 0;
                break;
//class B
            case 16:
                mask1 = 255;
                mask2 = 255;
                mask3 = 0;
                mask4 = 0;
                break;
            case 17:
                mask1 = 255;
                mask2 = 255;
                mask3 = 128;
                mask4 = 0;
                break;
            case 18:
                mask1 = 255;
                mask2 = 255;
                mask3 = 192;
                mask4 = 0;
                break;
            case 19:
                mask1 = 255;
                mask2 = 255;
                mask3 = 224;
                mask4 = 0;
                break;
            case 20:
                mask1 = 255;
                mask2 = 255;
                mask3 = 240;
                mask4 = 0;
                break;
            case 21:
                mask1 = 255;
                mask2 = 255;
                mask3 = 248;
                mask4 = 0;
                break;
            case 22:
                mask1 = 255;
                mask2 = 255;
                mask3 = 252;
                mask4 = 0;
                break;
            case 23:
                mask1 = 255;
                mask2 = 255;
                mask3 = 254;
                mask4 = 0;
                break;
//class C
            case 24:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 0;
                break;
            case 25:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 128;
                break;
            case 26:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 192;
                break;
            case 27:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 224;
                break;
            case 28:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 240;
                break;
            case 29:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 248;
                break;
            case 30:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 252;
                break;
            case 31:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 254;
                break;
            case 32:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
        }
        octet1 = Octet1 & mask1;
        octet2 = Octet2 & mask2;
        octet3 = Octet3 & mask3;
        octet4 = Octet4 & mask4;
        return "Network address : " + octet1 + "." + octet2 + "." + octet3 + "." + octet4;
    }

    String showBroadcastAddress(int Octet1, int Octet2, int Octet3, int Octet4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octet1 = Integer.parseInt(toBinary(Octet1));
        octet2 = Integer.parseInt(toBinary(Octet2));
        octet3 = Integer.parseInt(toBinary(Octet3));
        octet4 = Integer.parseInt(toBinary(Octet4));
        switch (mask) {
            case 1:
                mask1 = 127;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 2:
                mask1 = 63;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 3:
                mask1 = 31;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 4:
                mask1 = 15;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 5:
                mask1 = 7;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 6:
                mask1 = 3;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 7:
                mask1 = 1;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            //class A
            case 8:
                mask1 = 0;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 9:
                mask1 = 0;
                mask2 = 127;
                mask3 = 255;
                mask4 = 255;
                break;
            case 10:
                mask1 = 0;
                mask2 = 63;
                mask3 = 255;
                mask4 = 255;
                break;
            case 11:
                mask1 = 0;
                mask2 = 31;
                mask3 = 255;
                mask4 = 255;
                break;
            case 12:
                mask1 = 0;
                mask2 = 15;
                mask3 = 255;
                mask4 = 255;
                break;
            case 13:
                mask1 = 0;
                mask2 = 7;
                mask3 = 255;
                mask4 = 255;
                break;
            case 14:
                mask1 = 0;
                mask2 = 3;
                mask3 = 255;
                mask4 = 255;
                break;
            case 15:
                mask1 = 0;
                mask2 = 1;
                mask3 = 255;
                mask4 = 255;
                break;
//class B
            case 16:
                mask1 = 0;
                mask2 = 0;
                mask3 = 255;
                mask4 = 255;
                break;
            case 17:
                mask1 = 0;
                mask2 = 0;
                mask3 = 127;
                mask4 = 255;
                break;
            case 18:
                mask1 = 0;
                mask2 = 0;
                mask3 = 63;
                mask4 = 255;
                break;
            case 19:
                mask1 = 0;
                mask2 = 0;
                mask3 = 31;
                mask4 = 255;
                break;
            case 20:
                mask1 = 0;
                mask2 = 0;
                mask3 = 15;
                mask4 = 255;
                break;
            case 21:
                mask1 = 0;
                mask2 = 0;
                mask3 = 7;
                mask4 = 255;
                break;
            case 22:
                mask1 = 0;
                mask2 = 0;
                mask3 = 3;
                mask4 = 255;
                break;
            case 23:
                mask1 = 0;
                mask2 = 0;
                mask3 = 1;
                mask4 = 255;
                break;
//class C
            case 24:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 255;
                break;
            case 25:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 127;
                break;
            case 26:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 63;
                break;
            case 27:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 31;
                break;
            case 28:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 15;
                break;
            case 29:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 7;
                break;
            case 30:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 3;
                break;
            case 31:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 1;
                break;
            case 32:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
        }

        octet1 = Octet1 | mask1;
        octet2 = Octet2 | mask2;
        octet3 = Octet3 | mask3;
        octet4 = Octet4 | mask4;
        return "Boardcast address : " + octet1 + "." + octet2 + "." + octet3 + "." + octet4;
    }

    String showBroadcastAddressRange(int Octet1, int Octet2, int Octet3, int Octet4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octet1 = Integer.parseInt(toBinary(Octet1));
        octet2 = Integer.parseInt(toBinary(Octet2));
        octet3 = Integer.parseInt(toBinary(Octet3));
        octet4 = Integer.parseInt(toBinary(Octet4));
        switch (mask) {
            case 1:
                mask1 = 127;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 2:
                mask1 = 63;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 3:
                mask1 = 31;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 4:
                mask1 = 15;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 5:
                mask1 = 7;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 6:
                mask1 = 3;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 7:
                mask1 = 1;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            //class A
            case 8:
                mask1 = 0;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 9:
                mask1 = 0;
                mask2 = 127;
                mask3 = 255;
                mask4 = 255;
                break;
            case 10:
                mask1 = 0;
                mask2 = 63;
                mask3 = 255;
                mask4 = 255;
                break;
            case 11:
                mask1 = 0;
                mask2 = 31;
                mask3 = 255;
                mask4 = 255;
                break;
            case 12:
                mask1 = 0;
                mask2 = 15;
                mask3 = 255;
                mask4 = 255;
                break;
            case 13:
                mask1 = 0;
                mask2 = 7;
                mask3 = 255;
                mask4 = 255;
                break;
            case 14:
                mask1 = 0;
                mask2 = 3;
                mask3 = 255;
                mask4 = 255;
                break;
            case 15:
                mask1 = 0;
                mask2 = 1;
                mask3 = 255;
                mask4 = 255;
                break;
//class B
            case 16:
                mask1 = 0;
                mask2 = 0;
                mask3 = 255;
                mask4 = 255;
                break;
            case 17:
                mask1 = 0;
                mask2 = 0;
                mask3 = 127;
                mask4 = 255;
                break;
            case 18:
                mask1 = 0;
                mask2 = 0;
                mask3 = 63;
                mask4 = 255;
                break;
            case 19:
                mask1 = 0;
                mask2 = 0;
                mask3 = 31;
                mask4 = 255;
                break;
            case 20:
                mask1 = 0;
                mask2 = 0;
                mask3 = 15;
                mask4 = 255;
                break;
            case 21:
                mask1 = 0;
                mask2 = 0;
                mask3 = 7;
                mask4 = 255;
                break;
            case 22:
                mask1 = 0;
                mask2 = 0;
                mask3 = 3;
                mask4 = 255;
                break;
            case 23:
                mask1 = 0;
                mask2 = 0;
                mask3 = 1;
                mask4 = 255;
                break;
//class C
            case 24:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 255;
                break;
            case 25:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 127;
                break;
            case 26:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 63;
                break;
            case 27:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 31;
                break;
            case 28:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 15;
                break;
            case 29:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 7;
                break;
            case 30:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 3;
                break;
            case 31:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 1;
                break;
            case 32:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
        }

        octet1 = Octet1 | mask1;
        octet2 = Octet2 | mask2;
        octet3 = Octet3 | mask3;
        octet4 = Octet4 | mask4;
        int range = octet4 - 1;
        return octet1 + "." + octet2 + "." + octet3 + "." + range;
    }

    String showNetAddressRange(int Octet1, int Octet2, int Octet3, int Octet4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octet1 = Integer.parseInt(toBinary(Octet1));
        octet2 = Integer.parseInt(toBinary(Octet2));
        octet3 = Integer.parseInt(toBinary(Octet3));
        octet4 = Integer.parseInt(toBinary(Octet4));
        switch (mask) {
            case 1:
                mask1 = 128;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 2:
                mask1 = 192;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 3:
                mask1 = 224;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 4:
                mask1 = 240;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 5:
                mask1 = 248;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 6:
                mask1 = 252;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 7:
                mask1 = 254;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            //class A
            case 8:
                mask1 = 255;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 9:
                mask1 = 255;
                mask2 = 128;
                mask3 = 0;
                mask4 = 0;
                break;
            case 10:
                mask1 = 255;
                mask2 = 192;
                mask3 = 0;
                mask4 = 0;
                break;
            case 11:
                mask1 = 255;
                mask2 = 224;
                mask3 = 0;
                mask4 = 0;
                break;
            case 12:
                mask1 = 255;
                mask2 = 240;
                mask3 = 0;
                mask4 = 0;
                break;
            case 13:
                mask1 = 255;
                mask2 = 248;
                mask3 = 0;
                mask4 = 0;
                break;
            case 14:
                mask1 = 255;
                mask2 = 252;
                mask3 = 0;
                mask4 = 0;
                break;
            case 15:
                mask1 = 255;
                mask2 = 254;
                mask3 = 0;
                mask4 = 0;
                break;
//class B
            case 16:
                mask1 = 255;
                mask2 = 255;
                mask3 = 0;
                mask4 = 0;
                break;
            case 17:
                mask1 = 255;
                mask2 = 255;
                mask3 = 128;
                mask4 = 0;
                break;
            case 18:
                mask1 = 255;
                mask2 = 255;
                mask3 = 192;
                mask4 = 0;
                break;
            case 19:
                mask1 = 255;
                mask2 = 255;
                mask3 = 224;
                mask4 = 0;
                break;
            case 20:
                mask1 = 255;
                mask2 = 255;
                mask3 = 240;
                mask4 = 0;
                break;
            case 21:
                mask1 = 255;
                mask2 = 255;
                mask3 = 248;
                mask4 = 0;
                break;
            case 22:
                mask1 = 255;
                mask2 = 255;
                mask3 = 252;
                mask4 = 0;
                break;
            case 23:
                mask1 = 255;
                mask2 = 255;
                mask3 = 254;
                mask4 = 0;
                break;
//class C
            case 24:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 0;
                break;
            case 25:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 128;
                break;
            case 26:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 192;
                break;
            case 27:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 224;
                break;
            case 28:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 240;
                break;
            case 29:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 248;
                break;
            case 30:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 252;
                break;
            case 31:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 254;
                break;
            case 32:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
        }
        octet1 = Octet1 & mask1;
        octet2 = Octet2 & mask2;
        octet3 = Octet3 & mask3;
        octet4 = Octet4 & mask4;
        int range = octet4 + 1;
        return octet1 + "." + octet2 + "." + octet3 + "." + range;
    }

    String showTotalHostNUseable(int mask) {
        String host = "";
        String usable = "";
        switch (mask) {
            case 1:
                host = "<html>Total Number of Hosts : 2,147,483,648";
                usable = "<br><br>Number of Usable Hosts : 2,147,483,646</html>";
                break;
            case 2:
                host = "<html>Total Number of Hosts : 1,073,741,824";
                usable = "<br><br>Number of Usable Hosts : 1,073,741,822</html>";
                break;
            case 3:
                host = "<html>Total Number of Hosts : 536,870,912";
                usable = "<br><br>Number of Usable Hosts : 536,870,910</html>";
                break;
            case 4:
                host = "<html>Total Number of Hosts : 268,435,456";
                usable = "<br><br>Number of Usable Hosts : 268,435,454</html>";
                break;
            case 5:
                host = "<html>Total Number of Hosts : 134,217,728";
                usable = "<br><br>Number of Usable Hosts : 134,217,726</html>";
                break;
            case 6:
                host = "<html>Total Number of Hosts : 67,108,864";
                usable = "<br><br>Number of Usable Hosts : 67,108,862</html>";
                break;
            case 7:
                host = "<html>Total Number of Hosts : 33,554,432";
                usable = "<br><br>Number of Usable Hosts : 33,554,430</html>";
                break;
//class A
            case 8:
                host = "<html>Total Number of Hosts : 16,777,216";
                usable = "<br><br>Number of Usable Hosts : 16,777,214</html>";
                break;
            case 9:
                host = "<html>Total Number of Hosts : 8,388,608";
                usable = "<br><br>Number of Usable Hosts : 8,388,606</html>";
                break;
            case 10:
                host = "<html>Total Number of Hosts : 4,194,304";
                usable = "<br><br>Number of Usable Hosts : 4,194,302</html>";
                break;
            case 11:
                host = "<html>Total Number of Hosts : 2,097,152";
                usable = "<br><br>Number of Usable Hosts : 2,097,150</html>";
                break;
            case 12:
                host = "<html>Total Number of Hosts : 1,048,576";
                usable = "<br><br>Number of Usable Hosts : 1,048,574</html>";
                break;
            case 13:
                host = "<html>Total Number of Hosts : 524,288";
                usable = "<br><br>Number of Usable Hosts : 524,286</html>";
                break;
            case 14:
                host = "<html>Total Number of Hosts : 262,144";
                usable = "<br><br>Number of Usable Hosts : 262,142</html>";
                break;
            case 15:
                host = "<html>Total Number of Hosts : 131,072";
                usable = "<br><br>Number of Usable Hosts : 131,070</html>";
                break;
//class B
            case 16:
                host = "<html>Total Number of Hosts : 65,536";
                usable = "<br><br>Number of Usable Hosts : 65,534</html>";
                break;
            case 17:
                host = "<html>Total Number of Hosts : 32,768";
                usable = "<br><br>Number of Usable Hosts : 32,766</html>";
                break;
            case 18:
                host = "<html>Total Number of Hosts : 16,384";
                usable = "<br><br>Number of Usable Hosts : 16,382</html>";
                break;
            case 19:
                host = "<html>Total Number of Hosts : 8,192";
                usable = "<br><br>Number of Usable Hosts : 8,190</html>";
                break;
            case 20:
                host = "<html>Total Number of Hosts : 4,096";
                usable = "<br><br>Number of Usable Hosts : 4,094</html>";
                break;
            case 21:
                host = "<html>Total Number of Hosts : 2,048";
                usable = "<br><br>Number of Usable Hosts : 2,046</html>";
                break;
            case 22:
                host = "<html>Total Number of Hosts : 1,024";
                usable = "<br><br>Number of Usable Hosts : 1,022</html>";
                break;
            case 23:
                host = "<html>Total Number of Hosts : 512";
                usable = "<br><br>Number of Usable Hosts : 510</html>";
                break;
//class C
            case 24:
                host = "<html>Total Number of Hosts : 256";
                usable = "<br><br>Number of Usable Hosts : 254</html>";
                break;
            case 25:
                host = "<html>Total Number of Hosts : 128";
                usable = "<br><br>Number of Usable Hosts : 126</html>";
                break;
            case 26:
                host = "<html>Total Number of Hosts : 64";
                usable = "<br><br>Number of Usable Hosts : 62</html>";
                break;
            case 27:
                host = "<html>Total Number of Hosts : 32";
                usable = "<br><br>Number of Usable Hosts : 30</html>";
                break;
            case 28:
                host = "<html>Total Number of Hosts : 16";
                usable = "<br><br>Number of Usable Hosts : 14</html>";
                break;
            case 29:
                host = "<html>Total Number of Hosts : 8";
                usable = "<br><br>Number of Usable Hosts : 6</html>";
                break;
            case 30:
                host = "<html>Total Number of Hosts : 4";
                usable = "<br><br>Number of Usable Hosts : 2</html>";
                break;
            case 31:
                host = "<html>Total Number of Hosts : 2";
                usable = "<br><br>Number of Usable Hosts : 0</html>";
                break;
            case 32:
                host = "<html>Total Number of Hosts : 1";
                usable = "<br><br>Number of Usable Hosts : 0</html>";
                break;
        }
        return host + usable;
    }

    String showIPRange(int octet1, int octet2, int octet3, int octet4, int mask) {
        if (mask == 31 || mask == 32) {
            return "Usable Host IP Range : NA";
        } else {
            return "Usable Host IP Range : " + showNetAddressRange(octet1, octet2, octet3, octet4, mask) + " - " + showBroadcastAddressRange(octet1, octet2, octet3, octet4, mask);
        }
    }
}
