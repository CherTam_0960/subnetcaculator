
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kitti
 */
public class Main {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int octet1 = 10, octet2 = 0, octet3 = 0, octet4 = 0, mask = 15;
        NetId id = new NetId(octet1, octet2, octet3, octet4, mask);
        System.out.println(id);
        id.showNetAddress(octet1, octet2, octet3, octet4, mask);
        id.showIPRange(octet1, octet2, octet3, octet4, mask);
        id.showBroadcastAddress(octet1, octet2, octet3, octet4, mask);
        id.showTotalHostNUseable(mask);
        id.convertNetworkaddress(octet4);
        id.showClass(octet1);
        id.showCIRD();
        id.showType(octet1, octet2);
        System.out.println("----------------------------------");
        id.showShort();
        id.showBinaryID();
        id.showIntegerID();
        id.showHexID();

    }

}
